import numpy as np
from numpy import random as rand
from datetime import datetime
import time, sys, os, math, itertools
class seer:
    def __init__(self,seed):
        self.DEMONIC_NUMBER = 2054660605
        self.rng = rand.RandomState(0)
        suits  = ('acorns','bells','leaves','hearts','crescents')
        ranks  = ('infant','physicker','soldier','scout','officer',
                  'disciple','herald','warden','potentate')
        majors = ['the bat','the horse','the whale',
                  'the axe','the spear','the net',
                  'the barrister','the clerk','the executive',
                  'the moon','the sun','the earth',
                  'the dish','the vase','the urn',
                  'heaven','hell','void',
                  'hope','doom']
        product = itertools.product(ranks,suits)
        minors = ['the ' + c[0] + ' of ' + c[1] for c in product]
        self.cards = minors + majors
        self.width, self.height = os.get_terminal_size(0)
        self.delay = 0.04
    def center_print(self, string, cut = True):
        self.width, self.height = os.get_terminal_size(0)
        lines = string.split('\n')
        lens = [len(line) for line in lines]
        if cut:
            lines_copy = []
            for line_i in range(len(lines)):
                if lens[line_i] > self.width:
                    words = lines[line_i].split(' ')
                    wi = 0
                    num_words = len(words)
                    line_parts = []
                    line_pi = 0
                    while num_words > 0:
                        if line_pi == 0:
                            line_parts.append("")
                            p_len = 0
                        else:
                            line_parts.append("  ")
                            p_len = 2
                        while (num_words > 0 and
                               p_len + len(words[wi]) + 1 <= self.width):
                            line_parts[line_pi] += words[wi] + " "
                            p_len += len(words[wi]) + 1
                            wi += 1
                            num_words -= 1
                        line_parts[line_pi] = line_parts[line_pi][:-1]
                        line_pi += 1
                    for part in line_parts:
                        lines_copy.append(part)
                else:
                    lines_copy.append(lines[line_i])
            lines = lines_copy
            lens = [len(line) for line in lines]
        half_width = max((self.width - max(lens))//2,0)
        for line in lines:
            for i in range(half_width):
                print(' ', end = '')
            print(line.upper())
            time.sleep(self.delay)
    def print_line(self, length):
        l = ''
        for i in range(length):
            l += '⎯'
        self.center_print(l)
    def clear_screen(self):
        self.width, self.height = os.get_terminal_size(0)
        for i in range(self.height):
            print()
    def seer_print(self, string):
        length = len(string)
        choices = (" ","'","`","*",",",".",'"')
        for line_num in range(self.height):
            line_len = 0
            sparks = ""
            max_len = length + self.height - line_num
            while line_len < max_len and line_len < self.width:
                white_p = ((self.height-line_num)/self.height)**0.5
                ps = [white_p]
                for i in range(len(choices) - 1):
                    ps += [(1-white_p)/(len(choices) - 1)]
                sparks += self.rng.choice(choices,p=ps)
                line_len += 1
            sparks = sparks[:min(max_len,self.width)]
            self.center_print(sparks)
            sys.stdout.flush()
        self.print_line(self.width)
        self.center_print(string)
        self.print_line(self.width)
    def hash(self, question):
        seed = 2**16;
        now = datetime.now()
        seed += now.year*1000000
        seed += now.month*10000
        seed += now.day*100
        seed += now.hour
        if now.minute > 30:
            seed += self.DEMONIC_NUMBER
        sum = 0;
        for char_loc in range(len(question)):
            char = question[char_loc]
            seed += ord(char)
            seed *= self.DEMONIC_NUMBER - ord(char) - char_loc
            seed //= ord(char)
            sum += ord(char)
        seed *= sum
        seed = seed % (2**32)
        return seed
    def draw(self, num = 1):
        if num == 1:
            drawn = self.cards[self.rng.randint(0,len(self.cards))]
        else:
            drawn = []
            for i in range(num):
                cand = (self.cards[self.rng.randint(0,len(self.cards))])
                while cand and cand in drawn:
                    cand = (self.cards[self.rng.randint(0,len(self.cards))])
                drawn.append(cand)
        return drawn
    def answer(self,question):
        seed = self.hash(question)
        self.rng.seed(seed)
        return self.draw()
    def answer_spread(self,question,descs):
        for desc in descs:
            question += desc + str(self.DEMONIC_NUMBER)
        seed = self.hash(question)
        self.rng.seed(seed)
        return self.draw(len(descs))
    def ask(self):
        self.seer_print('what would you ask of the seer')
        question = input()
        answer = self.answer(question)
        self.seer_print(answer)
        self.center_print('press enter to continue')
        input()
    def spread_3(self):
        self.do_spread(['past','present','future'])
    def spread_5(self):
        self.do_spread(['past','present','future','cause','effect'])
    def spread_7(self):
        self.do_spread(['past','present','influences','identity',
                        'environment','advice','outcome','outcome'])
    def spread_10(self):
        self.do_spread(['condition','obstacles','best-case','cause',
                        'recent past','near future','identity',
                        'environment','hopes and fears','outcome'])
    def spread_12(self):
        self.do_spread(['aries/self','taurus/money',
                        'gemini/day-to-day','cancer/family',
                        'leo/pleasure','virgo/work and health',
                        'libra/relationships',
                        'scorpio/major changes',
                        'sagittarius/learning',
                        'capricorn/advances','aquarious/friends',
                        'pisces/burdens'])
    def spread(self):
        choices = ['3 cards: time','5 cards: causal','7 card: horseshoe',
                   '10 card: cross','12 card: zodiac','back']
        functions = [self.spread_3,self.spread_5,self.spread_7,
                     self.spread_10,self.spread_12]
        self.menu('spreads',choices,functions)
    def do_spread(self,descs):
        self.seer_print('what would you ask of the seer')
        question = input()
        answers = self.answer_spread(question, descs)
        self.seer_print(descs[0] + ': ' + answers[0])
        for index in range(1,len(answers)):
            self.center_print(descs[index] + ': ' + answers[index])
            self.print_line(self.width)
        self.center_print('press enter to continue')
        input()
    def learn(self):
        choices = ['suits','ranks','major symbols',
                   'spreads','the seer','back']
        functions = [self.learn_suits,self.learn_ranks,self.learn_majors,
                     self.learn_spreads,self.learn_about]
        self.menu('learn',choices,functions)
    def do_learn(self, title, string):
        self.clear_screen()
        self.print_line(self.width)
        self.center_print(title)
        self.print_line(self.width)
        self.center_print(string)
        self.center_print('press enter to continue')
        input()
    def learn_spreads(self):
        return
    def learn_about(self):
        return
    def learn_suits(self):
        self.do_learn('suits','acorns represent the truth\n' +
                          'leaves represent appearances\n' +
                          'hearts represent emotions\n' +
                          'crescents represent ideas\n' +
                          'bells represent the physical\n')
    def learn_ranks(self):
        self.do_learn('ranks', 'the infant represents a beginning\n' +
                          'the physicker represents healing and expertise\n' +
                          'the soldier represents opposition ' +
                          'and persistance\n' +
                          'the scout represents oblique strategies ' +
                          'and an open mind\n' +
                          'the officer represents decisions ' +
                          'and responsibility\n' +
                          'the disciple represents obedience and learning\n' +
                          'the herald represents leading' +
                          'and communication\n' +
                          'the warden represents protection and inaction\n' +
                          'the potentate represents control and force\n')
    def learn_majors(self):
        self.do_learn('major symbols','the bat is flight and ' +
                          'blindness and blood and night\n' +
                          'the horse is speed and ' +
                          'strength and vulnerablity and luck\n' +
                          'the whale is size and ' +
                          'water and music and hunger\n' +
                          'the axe is cutting and ' +
                          'wood and usefulness and leverage\n' +
                          'the spear is male and ' +
                          'piercing and length and teamwork\n' +
                          'the net is female and ' +
                          'capture and confusion and teamwork\n' +
                          'the barrister is words and ' +
                          'facts and decisions and justice\n' +
                          'the clerk is money and ' +
                          'numbers and politeness and work\n' +
                          'the executive is power and ' +
                          'privilege and creativity and theft\n' +
                          'the moon is reflections and ' +
                          'cold and inevitablity and time\n' +
                          'the sun is day and ' +
                          'heat and life and worship\n' +
                          'the earth is home and ' +
                          'nature and movement and stability\n' +
                          'the dish is food and ' +
                          'presentation and need and aid\n' +
                          'the vase is beauty and ' +
                          'lacking and plants and love\n' +
                          'the urn is death and ' +
                          'art and ceremony and fire\n' +
                          'heaven is reward and ' +
                          'piety and eternity and promise\n' +
                          'hell is punishment and ' +
                          'impiety and eternity and torture\n' +
                          'void is the unknown and ' +
                          'chaos and choice and origin\n' +
                          'hope is good and uncertainty\n' +
                          'doom is evil and destiny\n')
    def quickdraw(self):
        self.seer_print(self.answer(str(datetime.now())))
        self.center_print('press enter to continue')
        input()
    def menu(self, title, choices, functions):
        while True:
            self.clear_screen()
            self.print_line(self.width)
            self.center_print(title)
            self.print_line(self.width)
            choices_str = ""
            for index in range(len(choices)):
                choices_str += str(index + 1) + '. ' + choices[index] + '\n'
            self.center_print(choices_str)
            selection = input()
            try:
                sel_num = int(selection) - 1
            except (ValueError):
                sel_num = choices.index(selection)
            except:
                continue
            finally:
                if sel_num == len(choices) - 1:
                    self.clear_screen()
                    break
                else:
                    functions[sel_num]()
    def main_menu(self):
        choices = ['quick draw','ask','spread','learn','exit']
        functions = [self.quickdraw,self.ask,self.spread,self.learn]
        self.menu('the seer',choices,functions)
seer = seer(0)
seer.main_menu()
